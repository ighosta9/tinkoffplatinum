let
    cashSlider     = document.querySelector('#calc__sliderCash'),
    yearSlider     = document.querySelector('#calc__sliderYear')
    sliders        = [cashSlider, yearSlider];

function Slider(slider) {
  this.slider = slider;
  slider.addEventListener('input', function() {
    this.updateSliderOutput();
    this.updateSliderLevel();
  }.bind(this), false);
  
  this.level = function() {
    let level = this.slider.querySelector('.slider__input');
    return level.value;
  }
  
  this.levelString = function() {
    return parseInt(this.level());
  }
  
  this.updateSliderOutput = function() {
    let thumb = this.slider.querySelector('.track__thumb');

    if (slider === cashSlider) {
      let output = this.slider.querySelector('.slider__output');
      output.value = this.levelString() * 4460 + ' ₽';
    } else if (slider === yearSlider) {
      let output = this.slider.querySelector('.slider__output');

      let year = output.value.split(' ')
      let percent = parseInt(thumb.style.left)

      if (percent) {
        console.log(percent)
        if (percent <= 2) {
          output.value = '1 год';
        } else if (percent < 8) {
          output.value = '2 года';
        } else if (percent < 16) {
          output.value = '3 года';
        } else if (percent < 24) {
          output.value = '4 года';
        } else if (percent < 32) {
          output.value = '5 лет';
        } else if (percent < 40) {
          output.value = '6 лет';
        } else if (percent < 48) {
          output.value = '7 лет';
        } else if (percent < 56) {
          output.value = '8 лет';
        } else if (percent < 64) {
          output.value = '9 лет';
        } else if (percent < 72) {
          output.value = '10 лет';
        } else if (percent < 80) {
          output.value = '11 лет';
        } else if (percent < 88) {
          output.value = '12 лет';
        } else if (percent < 96) {
          output.value = '13 лет';
        } else if (percent < 100) {
          output.value = '14 лет';
        }
      }
    }

    thumb.style.left = this.levelString() + '%';
  }
  
  this.updateSlider = function(num) {
    let input = this.slider.querySelector('.slider__input');
    input.value = num;
  }
  
  this.updateSliderLevel =function() {
    let level = this.slider.querySelector('.track__level');
    level.style.width = this.levelString() + '%';
  }
}

sliders.forEach(function(slider) {
  new Slider(slider);
});